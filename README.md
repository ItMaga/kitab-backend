# KITAB BACKEND

## Need install docker 
[Docker Install](https://docs.docker.com/engine/install/)

### Build docker
`docker-compose build`

### Start docker
`docker-compose up`

### Start docker with build
`docker-compose up --build`

### Stop docker
`CTRL + C`

### When has any unclosed containers 
`docker-compose down`