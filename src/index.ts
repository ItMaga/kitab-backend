import express, { Express, NextFunction, Request, Response } from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import dotenv from 'dotenv';
import 'reflect-metadata';
import authRoutes from './auth/urls';
import { createConnection } from 'typeorm';
import Logging from './utils/Logging';

dotenv.config();

const PORT = process.env.PORT || 9000;
const app: Express = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req: Request, res: Response) => {
    res.send('<h1>Hello from the TypeScript world!</h1>');
});

app.use((req: Request, res: Response, next: NextFunction): void => {
    res.on('finish', () => {
        Logging.request(req.method, req.url, res.statusCode);
    });

    next();
});

(async () => {
    try {
        await createConnection();
        console.log('Database connect successfully 🏁');
    } catch (e) {
        console.log(e);
    }
})();

app.use('/', authRoutes);
app.listen(PORT, () => console.log(`Running on ${PORT} ⚡`));
