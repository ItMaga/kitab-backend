import { Router } from 'express';
import AuthController from './controllers/AuthController';
import RegistrationController from './controllers/RegistrationController';

const authController = new AuthController();
const registrationController = new RegistrationController();
const router = Router();

router.post('/login', authController.login);
router.post('/registration', registrationController.registration);

export default router;
