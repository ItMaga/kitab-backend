import { Request, Response } from 'express';
import { Validator } from '../../utils/Validator';
import { User } from '../../entity/User';
import { getRepository } from 'typeorm';

export default class RegistrationController {
    public async registration(req: Request, res: Response): Promise<void | Response> {
        try {
            const errors = await Validator.validateEntity(User, req);
            if (errors) return res.status(400).send(errors);

            const userRepository = await getRepository(User);
            const { email } = req.body;
            const hasUserExistEmail = await userRepository.findOne({ where: { email } });

            if (hasUserExistEmail) return res.status(400).send('Email already used');
            try {
                await userRepository.insert(req.body);
                res.status(200).send('success');
            } catch (e) {
                res.status(400).send(e);
            }
        } catch (e) {
            res.status(400).send(e);
        }
    }
}
