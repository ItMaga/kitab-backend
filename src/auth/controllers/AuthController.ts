import { Request, Response } from 'express';
import { User } from '../../entity/User';
import { getRepository } from 'typeorm';
import { Validator } from '../../utils/Validator';
import { JwtPayload } from '../../types/JwtPayload';
import { createJwt } from '../../utils/createJwt';

class AuthController {
    public async login(req: Request, res: Response): Promise<void | Response> {
        try {
            const isValidKeys = Validator.validateKeys(req, ['email', 'password']);
            const errors = await Validator.validateEntity(User, req, { skipMissingProperties: true });
            if (!isValidKeys || errors) {
                return res.status(400).send(errors || 'ERROR');
            }

            const userRepository = await getRepository(User);
            const userForLogin = await userRepository.findOne({ ...req.body });

            if (userForLogin) {
                const jwtPayload: JwtPayload = {
                    id: userForLogin.id,
                    email: userForLogin.email,
                };
                const token = createJwt(jwtPayload);
                res.status(200).send({ token });
            } else {
                res.status(400).send('NOT FIND');
            }
        } catch (e) {
            res.status(400).send(e);
        }
    }
}

export default AuthController;
