import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Length, IsEmail, IsNotEmpty } from 'class-validator';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    @Length(0, 100)
    @IsNotEmpty()
    username!: string;

    @Column()
    @IsEmail()
    @IsNotEmpty()
    email!: string;

    @Column()
    @IsNotEmpty()
    password!: string;
}
