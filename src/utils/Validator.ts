import { Request } from 'express';
import { validate, ValidatorOptions } from 'class-validator';
import { Entities } from '../types/Entities';
import Dict = NodeJS.Dict;

export class Validator {
    public static validateKeys(req: Request, keys: Array<string>): boolean {
        return keys.every((key) => Object.prototype.hasOwnProperty.call(req.body, key));
    }

    public static async validateEntity(
        Entity: Entities,
        req: Request,
        validateOptions?: ValidatorOptions,
    ): Promise<Dict<string> | undefined | string> {
        const item = new Entity();
        Object.assign(item, req.body);
        const valid = await validate(item, validateOptions);

        if (valid.length) return valid[0].constraints;
        return '';
    }
}
