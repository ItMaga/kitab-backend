export default class Logging {
    private static get timeStamp() {
        return new Date().toUTCString();
    }

    public static request(method: string, url: string, status: number): void {
        console.log(`[INFO] ${this.timeStamp} METHOD: [${method}] - URL: [${url}] - STATUS: [${status}]`);
    }
}
