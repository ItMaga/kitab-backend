import jwt from 'jsonwebtoken';

import { JwtPayload } from '../types/JwtPayload';

export const createJwt = (payload: JwtPayload): string => {
    return jwt.sign(payload, process.env.JWT_SECRET_KEY!);
};
