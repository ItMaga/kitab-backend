FROM node:14.17.4-alpine

WORKDIR /app

COPY ./package.json .
COPY ./yarn.lock .

RUN yarn

COPY . .

EXPOSE 9000

CMD [ "yarn", "serve" ]